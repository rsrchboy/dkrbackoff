/*
Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package dkrbackoff

import (
	"context"

	"github.com/cenkalti/backoff/v4"
	"github.com/docker/docker/client"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog/log"
)

// DockerOpFunc performs the docker operation requested, while wrapped in
// backoff.Retry()
type DockerOpFunc func(context.Context, *client.Client) error

// DockerOp takes a DockerOpFunc and call it, wrapping it with backoff.Retry()
// and setting it to cancel-on-context-cancel.  The wrapper func also creates
// a docker client and handles deciding if a failed operation should be
// retried.
func DockerOp(ctx context.Context, op DockerOpFunc) error {

	// we don't actually do anything with this yet, but we may as well start
	// out pointed in a decent direction
	b := backoff.WithContext(backoff.NewExponentialBackOff(), ctx)
	i := 1

	wrapped := func() error {
		docker, cleanup, err := DockerClient(ctx)
		if err != nil {
			// DockerClient invokes ShouldRetry itself
			return err
		}
		defer cleanup()

		timer := prometheus.NewTimer(metricOpDuration)
		metricOpsTotal.Inc()
		metricOpsCount.Inc()
		defer func() { timer.ObserveDuration(); metricOpsCount.Dec() }()

		if err = op(ctx, docker); err != nil {
			if retry, err := ShouldRetry(ctx, err); retry {
				log.Ctx(ctx).Warn().Err(err).Int("docker-try-number", i).Msg("docker operation failed; retrying")
				metricOpsRetried.Inc()
				return err
			}
			log.Ctx(ctx).Error().Err(err).Int("docker-try-number", i).Msg("docker operation failed; NOT retrying")
			return err
		}
		return nil
	}

	return backoff.Retry(wrapped, b)
}

// DockerClient fetches a... docker client
func DockerClient(ctx context.Context) (*client.Client, func(), error) {

	timer := prometheus.NewTimer(metricClientCreateDuration)
	defer func() { timer.ObserveDuration() }()

	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		log.Ctx(ctx).Error().Err(err).Msg("cannot create a docker client?!")
		return nil, nil, wrapError(ctx, err)
	}

	metricClientTotal.Inc()
	metricClientCount.Inc()
	return cli, func() {
		if err := cli.Close(); err != nil {
			log.Ctx(ctx).Warn().Err(err).Msg("error closing Docker client?!")
		}
		metricClientCount.Dec()
	}, nil
}

// wrapError is basically a lazy shortcut to ShouldRetry
func wrapError(ctx context.Context, err error) error {
	_, wrapped := ShouldRetry(ctx, err)
	return wrapped
}

// ShouldRetry determines if a given operation should be retried -- or if it's
// pointless.
//
// If we shouldn't retry (false), then err is wrapped via backoff.Permanent(),
// so as to keep from retrying the operation.
func ShouldRetry(ctx context.Context, err error) (bool, error) {

	// first, the good case
	if err == nil {
		return true, nil
	}

	stopErr := backoff.Permanent(err)

	// TODO: should we bail on any except connection-failed?
	if client.IsErrNotFound(err) {
		return false, stopErr
	}
	if client.IsErrUnauthorized(err) {
		return false, stopErr
	}
	if client.IsErrConnectionFailed(err) {
		return true, err
	}
	if client.IsErrNotImplemented(err) {
		return false, stopErr
	}
	if client.IsErrPluginPermissionDenied(err) {
		return false, stopErr
	}

	return true, err
}
