module gitlab.com/rsrchboy/dkrbackoff

go 1.16

require (
	github.com/cenkalti/backoff/v4 v4.1.2
	github.com/containerd/containerd v1.5.5 // indirect
	github.com/docker/docker v20.10.12+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/moby/term v0.0.0-20210619224110-3f7ff695adc6 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/prometheus/client_golang v1.12.1
	github.com/rs/zerolog v1.26.1
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	google.golang.org/grpc v1.40.0 // indirect
)
