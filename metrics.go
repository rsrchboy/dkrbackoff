/*
Copyright © 2021 Chris Weyl <cweyl@alumni.drew.edu>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package dkrbackoff

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

const metricsNamespace = "dkrbackoff"

var (
	// operations
	metricOpsTotal = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: metricsNamespace,
		Subsystem: "op",
		Name:      "total",
		Help:      "Total number of docker operations, ever.",
	})
	metricOpsCount = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: metricsNamespace,
		Subsystem: "op",
		Name:      "count",
		Help:      "Current count of docker operations.",
	})
	metricOpsRetried = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: metricsNamespace,
		Subsystem: "op",
		Name:      "retries_count",
		Help:      "Number of retries (errors) encountered handling docker operations.",
	})
	metricOpDuration = promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: metricsNamespace,
		Subsystem: "op",
		Name:      "duration",
		Help:      "Time it took to perform a docker op.",
	})
	// client
	metricClientCreateDuration = promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: metricsNamespace,
		Subsystem: "client",
		Name:      "create_duration",
		Help:      "Time it took to create a docker client. (mainly for curiosity right now)",
	})
	metricClientTotal = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: metricsNamespace,
		Subsystem: "client",
		Name:      "total",
		Help:      "Total number of docker clients, ever.",
	})
	metricClientCreationErrorCount = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: metricsNamespace,
		Subsystem: "client",
		Name:      "creation_errors",
		Help:      "Count of errors on trying to create a docker client.",
	})
	metricClientCount = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: metricsNamespace,
		Subsystem: "client",
		Name:      "count",
		Help:      "Current count of docker clients.",
	})
)
